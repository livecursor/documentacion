# Guía de inicio con Livecursor

## Antes de la instalación

Livecursor esta escrita en el lenguaje de programación ruby por tal motivo es indispensable que instales este en tu computador aquí te dejo el enlace de en el [sitio oficial](https://www.ruby-lang.org/es/downloads/)

Una vez tenga ruby se debe instalar una librería en c llamada __ncurses__ o __ncursesw__ necesaria para poder ejecutar la [gema curses](https://github.com/ruby/curses), una dependencia de Livecursor, según su sistema operativo la podrás encontrar con alguno de los siguientes nombres:

 - libncursesw5-dev
 - ncurses
 - lib32-ncurses 

Para más información de la librería puedes consultar [aquí](https://invisible-island.net/ncurses/ncurses.html).

Una vez instala ncurses solamente nos falta bajar la carpeta con las configuraciones iniciales, esta tiene como nombre livecursor y debe ponerse dentro de la carpeta de usuario, normalmente en sistemas GNU/Linux la carpeta livecursor debe estar al mismo nivel de la carpeta de documentos, escritorio o descargas. Si tiene [git](https://es.wikipedia.org/wiki/Git) y su sistema operativo es [estilo UNIX (MacOS, GNU/Linux)](https://es.wikipedia.org/wiki/Unix-like) solo se tiene que abrir la terminal y correr este comando:

```console
 git clone https://gitlab.com/livecursor/livecursor.git ~/livecursor
```

Listo!!! ya tenemso todo lo que necesitamos para instalar Livecursor.

## Instalación

Solo basta con correr este comando en la terminal:

```console
  gem install livecursor
```
Listo!! Esto no podría ser más simple (Por lo menos en sistemas GNU/Linux), Espero poder actualizar el proceso para
WIndows y MacOS proximamente.

Para robar que todo salio bien debes escribe en la terminal livecursor y deberías ver algo como Esto

![livecursor ejemplo](img/livecursor-1.png)


Está es una pequeña muestra de lo que se puede hacer a nivel gráfico con Livecursor, le doy gracias a mi amiga Laura Zapata quién hizo la música con SuperCollider

https://youtu.be/S3EwajwpdIw

